package com.testng;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RegressionSuite {
	
	WebDriver driver;
	MethodRepository rep= new MethodRepository ();
	
	//Test by Partha
	@BeforeMethod
	 public void Applaunch() throws InterruptedException
	 {
		rep.browserApplicationLaunch();
	 }

	@Test
	public void Vrrifyvalidlogin() throws InterruptedException
	{
		Assert.assertEquals(rep.verifyLogin(), true);
	}
	
	@AfterMethod
	public void Appclose()
	{
		rep.Appclose();
	}
	
}
