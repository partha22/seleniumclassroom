package com.testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MethodRepository {

WebDriver driver;
	
	public void browserApplicationLaunch() throws InterruptedException
	{
		//system= class; setProperty= method
		
		System.setProperty("webdriver.chrome.driver", "D:\\tools\\chrome\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);
		
	}
	
	public boolean verifyLogin() throws InterruptedException
	{
	
		WebElement uname= driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys("dasd");
		
		WebElement pswd= driver.findElement(By.xpath("//input[@name='password']"));
		pswd.sendKeys("dasd");

        WebElement submit= driver.findElement(By.xpath("//input[@name='login']"));
		
		Thread.sleep(5000);
		
		//ACTION CLASS OBJECT
		
				Actions a1=new Actions(driver);
				a1.moveToElement(submit).click().build().perform();
				String ExpectedTitle ="Find a Flight: Mercury Tours:" ;
				   String ActualTitle = driver.getTitle();
				   if (ExpectedTitle.equals(ActualTitle))
				   {
				    //System.out.println("Pass");
				    return true;
				    }
				   else {
					  // System.out.println("Fail");
      return false;
	}
				   
	}
	
	
	public void Appclose()
	{
		driver.quit();
		
	}}

